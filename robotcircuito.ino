#include "Bot.h"
#include "SigueLineas.h"
#include "CogeLatas.h"


SigueLineas *robot = NULL;

char modo;
const char NO_ESTABLECIDO = 0;
const char SIGUE_LINEAS = 1;
const char COGE_LATAS = 2;

int tacoDerechoInicio;
int tacoIzquierdoInicio;

void setup()
{
  bot_setup();
  
  //Para hacer reset:
  bot_taco_izdo();
  bot_taco_dcho();
  
  Serial.begin(57600);
  
  modo = NO_ESTABLECIDO;
  
  tacoDerechoInicio = 0;
  tacoIzquierdoInicio = 0;
  
}

void loop()
{
  int tecla = bot_lee_tecla();

  //Para seleccionar el modo utilizando Putty.
  switch (tecla){
    case 's':
      robot = new SigueLineas();
      Serial.println("Modo siguelineas activado.");
      modo = SIGUE_LINEAS;
    break;
    case 'c':
      robot = new CogeLatas();
      Serial.println("Modo cogelatas activado.");
      modo = COGE_LATAS;
    break;
  }
  
  if (modo==NO_ESTABLECIDO){
    //SI NO ESTÁ EL MODO ESTABLECIDO, SALIMOS DE AQUÍ.
    return;
  }
  
   
  switch (tecla) {
    case 'q':
    case ' ':
      robot->setEstado(SigueLineas::PARAR);
      break;
    case 'r':
      if (modo==COGE_LATAS){
        robot->setEstado(CogeLatas::AVANCE_INICIAL);
      }
      else{
        robot->setEstado(SigueLineas::SIGUE_LINEAS_RAPIDO);
      }
      break;
    case 'm':
      robot->mostrarSensores();
      break;
    case 'b':
      Serial.println("Velocidad subida");
      robot->incrementarVelocidadBase(5);
      Serial.println(robot->getVelocidadBase());
      break;
    case 'v':
      Serial.println("Velocidad bajada");
      robot->incrementarVelocidadBase(-5);
      Serial.println(robot->getVelocidadBase());
      
      break;

  }
  
  robot->mandarTelemetria();
  robot->moverRobot();

}
