#include "GestorSensores.h"

GestorSensores::GestorSensores(){
  //Inicialización
  
  for (unsigned int i=0; i<TAMANO; i++){
    for (unsigned int sensor=0; sensor<CANTIDAD_SENSORES; sensor++){
      valores[i][sensor] = 0;
    }
  }
  siguienteIndice = 0;
}

void GestorSensores::addValores(int *s){
  
  for (unsigned int sensor=0; sensor<CANTIDAD_SENSORES; sensor++){
    valores[siguienteIndice][sensor] = s[sensor]; //Copiamos en nuestro array el array que recibimos.
  }
  
  siguienteIndice++;
  if (siguienteIndice>=TAMANO){
    siguienteIndice = 0;
  }
}

int GestorSensores::getSensor(int sensor){
  if (sensor>=CANTIDAD_SENSORES){
    return -1;
  }
  
  int sumatorio = 0;
  for (unsigned int i=0; i<TAMANO; i++){
    sumatorio+=valores[i][sensor];
  }
  
  return sumatorio/TAMANO;
}

void GestorSensores::getSensores(int *s){
  for (int i=0; i<CANTIDAD_SENSORES; i++){
    s[i] = getSensor(i);
  }
}

int GestorSensores::getUltimoSensor(int s){
  int ind = siguienteIndice-1; //Queremos acceder al índice anterior.
  if (ind<0) {ind=TAMANO-1;} //Pero si estaba en cero, valdría -1, lo ponemos al máximo.
  
  return valores[ind][s];
}

void GestorSensores::getUltimosSensores(int *s){
  for (int i=0; i<CANTIDAD_SENSORES; i++){
    s[i] = getUltimoSensor(i);
  }
}

void GestorSensores::imprimir(){
  for (int i=0; i<CANTIDAD_SENSORES; i++){
    Serial.print(getSensor(i));
    if (i!=CANTIDAD_SENSORES-1){
      Serial.print("-");
    }
  }
  Serial.println("");
  for (int i=0; i<CANTIDAD_SENSORES; i++){
    if (estaNegro(i)){
      Serial.print("N");
    }
    else{
      Serial.print("B");
    }
    
  }
  Serial.print(" Con ");
  Serial.print(cuantosNegrosHay());
  Serial.print(" sensores negros");
  Serial.println("");
  Serial.println("Array completo:");
  for (int j=0; j<TAMANO; j++){
    for (int i=0; i<CANTIDAD_SENSORES; i++){
      Serial.print(valores[j][i]);
      if (i!=CANTIDAD_SENSORES-1){
        Serial.print("-");
      }
    }
    Serial.println("");
  }
}

bool GestorSensores::estaNegro(int sensor, bool buscarSoloEnLosUltimos){
  if (buscarSoloEnLosUltimos==false){
    if (getSensor(sensor)>NEGRO){
      return true;
    }
  }
  else{
    if (getUltimoSensor(sensor)>NEGRO){
      return true;
    }
  }
  return false;
}

int GestorSensores::cuantosNegrosHay(bool buscarSoloEnLosUltimos){
  int negros = 0;
  bool hay = false;
  for (unsigned int i=0; i<CANTIDAD_SENSORES; i++){
    if (estaNegro(i,buscarSoloEnLosUltimos)){
      /*Serial.print(" -");
      Serial.print(i);
      Serial.print(" - ");*/
      negros++;
      //hay = true;
    }
    
  }
  if (hay){
    Serial.println("");
  }
  return negros;
}

void GestorSensores::leerSensores(){
  int s[CANTIDAD_SENSORES];
  bot_lee_sensores(s);
  addValores(s);
}
