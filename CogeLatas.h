#ifndef COGELATAS_H
#define COGELATAS_H

#include "Bot.h"
#include "SigueLineas.h"

class CogeLatas:public SigueLineas{
  private:
  
  int tacoIzqInicio, tacoDerInicio; //Para el estado SEGUIR_UN_POCO_MAS y RETROCEDER_UN_POCO
  int latasRestantes;
  static const int CANTIDAD_DE_LATAS = 32;
  
  unsigned long tiempoInicial;
  protected:
  
  void eventos(int evento);
  void reiniciarTacos();
  
  protected:
  
  void descansar(int ms=100);
  
  public:
  //Para evitar conflictos con estados de SigueLineas, se inicia en 10.
  enum {
    AVANCE_INICIAL = 10,
    SIGUE_LINEAS,
    SIGUE_LINEAS_LENTO,
    REGRESAR_A_LA_LINEA,
    RETROCEDER_POR_LA_LINEA,
    ROTAR,
    RETROCEDER,
    ORIENTAR_HACIA_LINEA,
    AVANZAR
        };
  
  CogeLatas();
  
  virtual void moverRobot();

  int getTacoIzquierdo(); //Obtienen los valores de los tacos restándoles los valores iniciales.
  int getTacoDerecho();
  
  float getDistanciaRuedaIzquierda();
  float getDistanciaRuedaDerecha();
  float getDistanciaMedia();
  
  virtual void setEstado(int estado); //Se reimplementa.
  void setLatasRestantes(int numeroLatas);
  int getLatasRestantes();
};

#endif
