#include "SigueLineas.h"

SigueLineas::SigueLineas(){
  //Inicializar variables.
  setEstado(PARAR);
  hayQueMostrarSensores = false;
  resetearValoresSigueLineas();
  setVelocidadBase(30);
  velocidadObjetivo = -1;
  aceleracion = -1;
}

void SigueLineas::setEstado(int estado){
  this->estado = estado;
}
int SigueLineas::getEstado(){
  return estado;
}

void SigueLineas::seguirLinea(){
  
  t1=micros();
  int s[6];
  gestorSensores.getUltimosSensores(s);
  
  int negros = gestorSensores.cuantosNegrosHay();
  
  if (negros==6){
    eventos(NEGRO_TOTAL);
  }
  
  if(negros>0) {
    e1=35-((s[0]*10L+s[1]*20L+s[2]*30L+s[3]*40L+s[4]*50L+s[5]*60L)/(s[0]+s[1]+s[2]+s[3]+s[4]+s[5]));//hay linea
  } else {//no hay linea
    eventos(NO_HAY_LINEA);
  
    if (e0<0) {
      e1=-25;
    } else {
      e1=25;
    }
  }
 
  Vi=velocidadBase+Kp*e1+(Kd*(e1-e0) / (t1-t0));
  Vd=velocidadBase-Kp*e1-(Kd*(e1-e0) / (t1-t0));
  bot_velocidad(Vi,Vd);
  t0=t1;
  e0=e1;
}

void SigueLineas::moverRobot(){
  gestorSensores.leerSensores();
  gestionarMRUA();
  
  switch (estado) {
    case PARAR:
      bot_velocidad(0, 0);
      break;
    case SIGUE_LINEAS_RAPIDO:
      seguirLinea();
      break;
  }
}

void SigueLineas::setVelocidadBase(float velocidadBase, float velocidadObjetivo, float aceleracion){
  this->velocidadBase = velocidadBase;
  this->velocidadObjetivo = velocidadObjetivo;
  this->aceleracion = aceleracion;
  actualizarValoresSigueLineas();
}

void SigueLineas::incrementarVelocidadBase(float cantidad, float velocidadObjetivo, float aceleracion){
  setVelocidadBase(velocidadBase+cantidad,velocidadObjetivo,aceleracion);
}

void SigueLineas::gestionarMRUA(){
  if (velocidadObjetivo==-1){ //Cuando es -1 no se quiere MRUA. Salimos.
    return;
  }
  
  //Si no se ha alcanzado la velocidad objetivo, aumentamos la velocidad.
  if (velocidadBase<velocidadObjetivo){
    incrementarVelocidadBase(aceleracion,velocidadObjetivo,aceleracion);
  }
  
  //A ahora comprobamos si hemos llegado a esa velocidad.
  if (velocidadBase>=velocidadObjetivo){
    setVelocidadBase(velocidadObjetivo);
    velocidadObjetivo = -1;
  }
  
}

void SigueLineas::mostrarSensores(){
  hayQueMostrarSensores = !hayQueMostrarSensores;
}

void SigueLineas::mandarTelemetria(){
  if (!hayQueMostrarSensores) {return;}
  
  Serial.print("Velocidad base: ");
  Serial.println(velocidadBase);

}

void SigueLineas::eventos(int evento){
}

float SigueLineas::getVelocidadBase(){
  return velocidadBase;
}


void SigueLineas::resetearValoresSigueLineas(){
  e0=e1=Vi=Vd=0;
  t0=t1=0;
}

void SigueLineas::actualizarValoresSigueLineas(){
  Kp=velocidadBase/28;
  Kd=velocidadBase*800;
}
