#include <Arduino.h>
#include "Bot.h"
#include "PinChangeInt.h"

//////////////////////////////////////////////////////////////////////////
// Constantes privadas (no compartidas)
//////////////////////////////////////////////////////////////////////////

const static float RADIO_RUEDA = 1.5;
const static float PERIMETRO_RUEDA = RADIO_RUEDA*2*PI;

// ENTRADAS analogicas
const static int SEN1 = 0;
const static int SEN2 = 1;
const static int SEN3 = 2;
const static int SEN4 = 3;
const static int SEN5 = 4;
const static int SEN6 = 5;

// SALIDAS DIGITALES
const static int MIA = 6;   // PD6
const static int MIB = 5;   // PD5
const static int MDA = 11;  // PB3
const static int MDB = 3;   // PD3
const static int LED = 1;   // PD1
const static int LEDON = 7; // PD7

// ENTRADAS DIGITALES
const int TACO_IZDO = 2; // SEN8 = PD2
const int TACO_DCHO = 8; // SEN7 = PB0

//////////////////////////////////////////////////////////////////////////
// Variables privadas (no compartidas)
//////////////////////////////////////////////////////////////////////////

volatile static int gTacoIzdo = 0;
volatile static int gTacoDcho = 0;

//////////////////////////////////////////////////////////////////////////
// Funciones privadas (no compartidas)
//////////////////////////////////////////////////////////////////////////

static void intTacoIzdo()
{
  gTacoIzdo++;
}

static void intTacoDcho()
{
  gTacoDcho++;
}

//////////////////////////////////////////////////////////////////////////
// Funciones públicas
//////////////////////////////////////////////////////////////////////////

void bot_setup()
{
  pinMode(MIA, OUTPUT);
  pinMode(MIB, OUTPUT);
  pinMode(MDA, OUTPUT);
  pinMode(MDB, OUTPUT);
  pinMode(LED, OUTPUT);
  pinMode(LEDON, OUTPUT);
  
  pinMode(TACO_IZDO, INPUT);
  pinMode(TACO_DCHO, INPUT);
  
  PCintPort::attachInterrupt(TACO_IZDO, intTacoIzdo, CHANGE);
  PCintPort::attachInterrupt(TACO_DCHO, intTacoDcho, CHANGE);
  
  digitalWrite(LED, LOW);    // Apaga el led rojo
  digitalWrite(LEDON, HIGH); // Enciende las luces de los sensores
}

int bot_lee_tecla()
{
  int b = -1;
  if (Serial.available() > 0) {
    b = Serial.read();
  }
  return b;
}

void bot_lee_sensores(int s[])
{
  s[0] = analogRead(SEN1);
  s[1] = analogRead(SEN2);
  s[2] = analogRead(SEN3);
  s[3] = analogRead(SEN4);
  s[4] = analogRead(SEN5);
  s[5] = analogRead(SEN6);
}

//Han sido invertidos los nombres de las funciones bot_taco_dcho() y bot_taco_izdo()
int bot_taco_dcho()
{
  // Copia los valores de los tacometros atomicamente
  cli();
  int t = gTacoIzdo;
  sei();
  return t;
}

int bot_taco_izdo()
{
  // Copia los valores de los tacometros atomicamente
  cli();
  int t = gTacoDcho;
  sei();
  return t;
}

float bot_distancia_taco_izdo(int inicial){
  float incrementoTacoIzdo = bot_taco_izdo()-inicial;
  return incrementoTacoIzdo/8*PERIMETRO_RUEDA;
}

float bot_distancia_taco_dcho(int inicial){
  float incrementoTacoDcho = bot_taco_dcho()-inicial;
  return incrementoTacoDcho/8*PERIMETRO_RUEDA;
}

void bot_velocidad(float izdaF, float dchaF)
{

  int izda = recortar_velocidad(izdaF);
  int dcha = recortar_velocidad(dchaF);
  // Motor izquierdo
  if (izda == 0) {
    digitalWrite(MIA, HIGH);
    digitalWrite(MIB, HIGH);
  } else if (izda > 0) {
    digitalWrite(MIA, HIGH);
    analogWrite(MIB, 255 - izda);
  } else {
    analogWrite(MIA, 255 + izda);
    digitalWrite(MIB, HIGH);
  }
  
  // Motor derecho
  if (dcha == 0) {
    digitalWrite(MDA, HIGH);
    digitalWrite(MDB, HIGH);
  } else if (dcha > 0) {
    digitalWrite(MDA, HIGH);
    analogWrite(MDB, 255 - dcha);
  } else {
    analogWrite(MDA, 255 + dcha);
    digitalWrite(MDB, HIGH);
  }
}
int recortar_velocidad(float velocidad){
  if (velocidad>255){
    velocidad = 255;
  }
  else if (velocidad<-255){
    velocidad = -255;
  }
  
  return velocidad;
}
