#ifndef GESTOR_SENSORES_HPP
#define GESTOR_SENSORES_HPP

#include "Arduino.h"
#include "Bot.h"

class GestorSensores{
  private:
  static const unsigned int TAMANO = 10;
  static const unsigned int CANTIDAD_SENSORES = 6;
  static const unsigned int NEGRO = 450;
  
  int valores[TAMANO][CANTIDAD_SENSORES];
  int siguienteIndice;
  
  public:
  GestorSensores();
  
  void addValores(int *s);
  void leerSensores();
  
  int getSensor(int indice);
  void getSensores(int *s);
  
  int getUltimoSensor(int indice);
  void getUltimosSensores(int *s);
  
  void imprimir();
  
  bool estaNegro(int sensor, bool buscarSoloEnLosUltimos=false);
  int cuantosNegrosHay(bool buscarSoloEnLosUltimos=false);
};

#endif

