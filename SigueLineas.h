#ifndef SIGUELINEAS_H
#define SIGUELINEAS_H

#include "Arduino.h"
#include "Bot.h"
#include "GestorSensores.h"
class SigueLineas{
  
  private:
  
  int e0, e1;
  float Vi, Vd; //Las velocidades son float para evitar el desbordamiento (Bot.h se encargará en convertirlas y recortarlas).
  unsigned long t0, t1;
  float velocidadBase, velocidadObjetivo, aceleracion, Kp, Kd;
  
  boolean hayQueMostrarSensores;
  
  public:
  enum {PARAR, SIGUE_LINEAS_RAPIDO};
  enum {NO_HAY_LINEA, NEGRO_TOTAL}; //Eventos
  static const int MUY_NEGRO=900, NEGRO=600;
  
  SigueLineas();
  virtual void setEstado(int estado);
  int getEstado();
  void setVelocidadBase(float velocidadBase, float velocidadObjetivo=-1, float aceleracion=-1);
  float getVelocidadBase();
  void incrementarVelocidadBase(float cantidad, float velocidadObjetivo=-1, float aceleracion=-1);
  void gestionarMRUA();
  
  void actualizarValoresSigueLineas();
  
  virtual void moverRobot();
  
  void mostrarSensores();
  void mandarTelemetria();
  
  protected:
  int estado; //Estado actual.
  GestorSensores gestorSensores;
  
  void seguirLinea();
  
  virtual void eventos(int evento);
  
  void resetearValoresSigueLineas();
  
};

#endif
