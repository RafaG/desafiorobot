#include "CogeLatas.h"

CogeLatas::CogeLatas():SigueLineas(){
  tacoDerInicio=tacoIzqInicio=0;
  reiniciarTacos();
}

void CogeLatas::moverRobot(){
  mandarTelemetria();
  gestorSensores.leerSensores();
  gestionarMRUA();

  switch (estado){
    case PARAR:
      bot_velocidad(0,0);
    break;
    
    case AVANCE_INICIAL:
      bot_velocidad(getVelocidadBase(),getVelocidadBase());
      if (getDistanciaMedia()>10){
        setEstado(SIGUE_LINEAS);
      }
    break;
    
    case SIGUE_LINEAS:
      seguirLinea();
      if (getDistanciaMedia()>=35){
        Serial.println("LENTO");
        setEstado(SIGUE_LINEAS_LENTO);
      }
    break;
    
    case SIGUE_LINEAS_LENTO:
      seguirLinea();
      if (gestorSensores.cuantosNegrosHay()==0){
        setLatasRestantes(getLatasRestantes()-1);
        if (getLatasRestantes()==0){
          Serial.print("Tiempo total: ");
          Serial.println((millis()-tiempoInicial)/1000.0);
          setEstado(PARAR);
        }
        else{
          setEstado(REGRESAR_A_LA_LINEA);
        }
      }
    break;
    
    case REGRESAR_A_LA_LINEA:
      bot_velocidad(-getVelocidadBase(),-getVelocidadBase());
      if (gestorSensores.cuantosNegrosHay()>0){
        setEstado(RETROCEDER_POR_LA_LINEA);
      }
    break;
    
    case RETROCEDER_POR_LA_LINEA:
      bot_velocidad(-getVelocidadBase(),-getVelocidadBase());
      if (getDistanciaMedia()>=5){
        setEstado(ROTAR);
      }
    break;
    
    case ROTAR:
      bot_velocidad(-getVelocidadBase(),0);
      if (getDistanciaMedia()>=8){
        setEstado(RETROCEDER);
      }
    break;
    
    case RETROCEDER:
      bot_velocidad(-getVelocidadBase(),-getVelocidadBase());
      if (getDistanciaMedia()>=40){
        setEstado(ORIENTAR_HACIA_LINEA);
      }
    break;
    
    case ORIENTAR_HACIA_LINEA:
      bot_velocidad(getVelocidadBase(),-getVelocidadBase());
      if (getDistanciaMedia()>=7){
        Serial.println("Fin rotacion.");
        setEstado(AVANZAR);
      }
      if (gestorSensores.cuantosNegrosHay()>0){
        Serial.println("Tocado negro.");
        setEstado(SIGUE_LINEAS);
      }
    break;
    
    case AVANZAR:
       bot_velocidad(getVelocidadBase(),getVelocidadBase());
       if (gestorSensores.cuantosNegrosHay()>0){
         setEstado(SIGUE_LINEAS);
         
       }
    break;
  }  
  
}

void CogeLatas::setEstado(int estadoNuevo){
  reiniciarTacos(); //Cada vez que se cambia de estado, se reinician los tacos.
  //Aprovechamos para configurar las velocidades dependiendo del estado.
  Serial.print("Estado nuevo: ");
  Serial.println(estadoNuevo);
  switch (estadoNuevo){
    case AVANCE_INICIAL:
      setLatasRestantes(CANTIDAD_DE_LATAS);
      tiempoInicial = millis();
      setVelocidadBase(60,100,2);
    break;
    
    case SIGUE_LINEAS:
      Serial.print("Tiempo parcial: ");
      Serial.println((millis()-tiempoInicial)/1000.0);
      setVelocidadBase(100);
    break;
    
    case SIGUE_LINEAS_LENTO:
      setVelocidadBase(60);
    break;
    
    case REGRESAR_A_LA_LINEA:
      descansar();
      setVelocidadBase(30);
    break;
    
    case RETROCEDER_POR_LA_LINEA:
      setVelocidadBase(60);
    break;
    
    case ROTAR:
      descansar();
      setVelocidadBase(50);
    break;
    
    case RETROCEDER:
      descansar();
      setVelocidadBase(90);
    break;
    
    case ORIENTAR_HACIA_LINEA:
      descansar(200);
      setVelocidadBase(40);
    break;
    
    case AVANZAR:
      descansar();
      setVelocidadBase(60);
    break;
  
  }
  
  SigueLineas::setEstado(estadoNuevo); //Clase padre.
  
  
}

void CogeLatas::eventos(int evento){
  if (evento==NO_HAY_LINEA){

  }
  
}

void CogeLatas::reiniciarTacos(){
  tacoIzqInicio = bot_taco_izdo();
  tacoDerInicio = bot_taco_dcho();
}

int CogeLatas::getTacoIzquierdo(){
  return bot_taco_izdo()-tacoIzqInicio;
}

int CogeLatas::getTacoDerecho(){
  return bot_taco_dcho()-tacoDerInicio;
}

float CogeLatas::getDistanciaRuedaDerecha(){
  return bot_distancia_taco_dcho(tacoDerInicio);
}

float CogeLatas::getDistanciaRuedaIzquierda(){
  return bot_distancia_taco_izdo(tacoIzqInicio);
}

float CogeLatas::getDistanciaMedia(){
  return (getDistanciaRuedaIzquierda()+getDistanciaRuedaDerecha())/2;
}

void CogeLatas::setLatasRestantes(int latasRestantes){
  this->latasRestantes = latasRestantes;
}

int CogeLatas::getLatasRestantes(){
  return latasRestantes;
}

void CogeLatas::descansar(int ms){
  bot_velocidad(0,0);
  delay(ms);
}
