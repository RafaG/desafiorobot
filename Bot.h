#ifndef __BOT_H__
#define __BOT_H__


void bot_setup();
int bot_lee_tecla();
void bot_lee_sensores(int s[]);
int bot_taco_izdo();
int bot_taco_dcho();
void bot_velocidad(float izda, float dcha);
float bot_distancia_taco_dcho(int tacoInicial);
float bot_distancia_taco_izdo(int tacoInicial);
int recortar_velocidad(float velocidad);

#endif
